# 不坑盒子

#### 介绍
一款助力高效办公的Office插件，支持Ms Office 2010以上 和 WPS，支持Word、Excel、PPT三件套。

#### 安装教程

1. 点击右边的[release](https://gitee.com/44886/BKOffice/releases)下载最新的安装包
2. 建议下载 ExE 文件，如果EXE文件在你的电脑上无法运行，那么下载 Zip包 暴力安装
3. 安装过程请退出360等杀毒软件

##### EXE安装办法：

双击即可安装

##### Zip包安装方法：

解压到没有空格的目录后，双击运行`_install.bat`即可。


